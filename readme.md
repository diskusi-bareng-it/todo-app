TODO App Practice
====

Server side is powered by Laravel 5.4.x

## Server Requirements

* PHP >= 5.6
* Zend-Opcache
* MCrypt PHP Extension
* GD PHP Extension
* Composer
* nginx (if possible)

## Database Requirements

* MySql >= 5.6.x

## Install Composer

```
    curl -sS https://getcomposer.org/installer | php
    mv composer.phar /usr/local/bin/composer

    ** For a faster package download, follow this article
    https://packagist.phpindonesia.id/
```

## Quick Start

```
    composer install (if it doesn't work then please do -> composer update --no-scripts)
    php artisan serve
```

## Create a Virtualhost In Your Local (for linux user only)

```
    # sample nginx virtualhost config

    server {

        listen   80;
        server_name localhost;

        location ^~ /todo-app {
            alias /var/www/todo-app/public;
            try_files $uri $uri/ @todo-app;
            index index.php index.html;

            location ~* "\/[0-9a-zA-Z]*\/assets(.+)$" {
                expires 90d;
                add_header Cache-Control "public";
                access_log off;
                try_files /todo-app/build$1 $uri;
            }

            location ~ \.php {
                fastcgi_pass   127.0.0.1:9000;
                fastcgi_split_path_info ^(.+\.php)(.*)$;
                fastcgi_param  SCRIPT_FILENAME    $request_filename;
                include /etc/nginx/fastcgi_params;
            }

        }

    }
```

## How to Install npm
```
    # Install the Latest npm version from the following article
    https://www.sitepoint.com/quick-tip-multiple-versions-node-nvm/
```

## How to Install Gulp and Run Gulp Related Task In Your Local
```
    # gulp needs to be installed globally
    npm install -g gulp

    # The below command will install all npm packages specified in package.json
    npm install

    # Before start working locally, run the following command in terminal
    gulp
```

# Naming Convention and Coding Policies

* Use english for folder, file, variable naming
* Localize all messages
* For blade or template file naming - snake_case
* For class file naming - camelCase
* For class & function  - camelCase -> scopeUserType()
* For variable          - snake_case -> $my_profile
* Use proper spaces between lines and use 4 spaces as tab
* Write if and else as follows:

```
    if (true) {

        // write your codes

    } else {

        // write your codes
    }
```

* Never use count or sizeof in for loop like:

```
    for ($i=0; $i<count($foo); $i++)                    -> incorrect
    $count = count($foo); for ($i=0; $i<$count; $i++)   -> correct
```

* Use meaningful name for variables, functions or classes
* For testing please use echo, var_dump, dd, etc. and remove those before you push it in repo
* Use single quote (') as much as you can rather double quote (")
* Use empty() rather ! (notation operator). Like:

```
    if (!empty($foo->bar)) {}   -> good way
    if ($foo->bar) {}           -> bad way

    or

    if (empty($foo->bar)) {}    -> good way
    if (!$foo->bar) {}          -> bad way
```

* Use sanitization function in POST action if necessary -> $postdata = postdata_strip_tags(Input::all());
* Use [] instead of array(). Like:

```
    $array = array(1,2,3,4,5)   -> incorrect
    $array = [1,2,3,4,5]        -> correct
```

* Capitalize first letter of every words except for label. Like:

    Button text for  'update profile'  should be   'Update Profile'

    Page title for   'project profile' should be   'Project Profile'

    Label for        'first name'      should be   'First name'